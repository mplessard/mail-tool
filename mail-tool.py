''' Mail tool is a simple software developed to help people, with gmail addresses, sending mass emails with a csv file.
    Copyright (C) 2017  Marie-Pier Lessard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.'''

import csv
import smtplib
import time
import string
import re

from config import *

from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

with open(PATH_TO_EMAIL_MESSAGE, 'r') as message_file:
    message = message_file.read()


with open(PATH_TO_ATTACHMENT, 'rb') as attachment_file:
    part = MIMEApplication(attachment_file.read(), Name=basename(PATH_TO_ATTACHMENT))
    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(PATH_TO_ATTACHMENT)

with open(PATH_TO_CSV_INPUT_FILE, 'r') as csv_file:
    reader = csv.reader(csv_file, delimiter=',', quotechar='|')
    not_sent = ''

    for line in reader:
        personnalized_message = message

        company_name = line[0].strip()
        company_email = line[1].strip()

        if len(company_name) != 0 and re.match('[A-Za-z0-9\._%\+-]+@[A-Za-z0-9-\.]+', company_email):
            personnalized_message = personnalized_message.replace('[NOM_COMPAGNIE]', company_name)

            print('Sending email to {}...'.format(company_email))

            email = MIMEMultipart()

            email['From'] = FROM
            email['To'] = company_email
            email['Date'] = formatdate(localtime=True)
            email['Subject'] = SUBJECT

            email.attach(MIMEText(personnalized_message))
            email.attach(part)

            smtp = smtplib.SMTP('smtp.gmail.com', 587)
            smtp.ehlo()
            smtp.starttls()
            smtp.login(FROM, PASSWORD)
            smtp.sendmail(FROM, company_email, email.as_string())
            smtp.close()

            print('waiting...')
            time.sleep(10)
        else:
            print('email wasn\'t sent for {}.'.format(company_email))

            not_sent += ','.join(line) + '\n'

with open(PATH_TO_CSV_OUTPUT_FILE, 'a') as output:
    output.write(not_sent)

print('END OF PROGRAM.')
