# Mail Tool
Mail tool is a simple software developed to help people, with gmail adresses, sending mass emails with a csv file.

# Contributors
- Marie-Pier Lessard

# Requirements
- Python3

# How to start

- Configure your config.py with the following:
```
FROM = 'your@email.com'
PATH_TO_EMAIL_MESSAGE = 'your_message_to_send.txt'
PATH_TO_ATTACHMENT = 'your_path_to_the_attachment_to_send.pdf'
PATH_TO_CSV_INPUT_FILE = 'your_list_of_email.csv'
PATH_TO_CSV_OUTPUT_FILE = 'your_path_to_output_emails_with_error.csv'
PASSWORD = 'your_password'
SUBJECT = 'your_email_subject'
```

- Your CSV file must have the following style:
```
Name of compagnie,their@email.com
```

- Start the script script and enjoy!
```
$ python3 email_script.py
```
